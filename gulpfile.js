paths = {
    development: "app/",
    production: "build/"
}

/**
 * Пути к компонентам
 * @type {Object}
 */
development = {
    path: "app/",
    scss: paths.development + "styles/",
    js: paths.development + "js/"
}

/**
 * Директории к компонентам на продакшине
 * @type {Object}
 */
production = {
    css: paths.production + "css/",
    js: paths.production + "js/",
    sprites: paths.production + "images/"
}

/**
 * Основные файлы и точки входа для всех компонент приложения
 * Объект в глобальной зоне видимости
 * @type {Object}
 */
files = {
    js: [
        development.js + "app.js",
        development.js + "templates.js",
        development.js + "controllers/*.js",
        development.js + "services/*.js",
        development.js + "directives/*.js"
    ],
    scss: {
        entryPoint: development.scss + "main.scss",
        files: [
            development.scss + "**/*.scss"
        ]
    },
    view: paths.development + "index.html",
    sprites: paths.development + "sprites/*.png",
    templates: paths.development + "templates/*.html"
}

/**
 * Вызываемые таски.
 * @type {Array}
 */
var tasks = [
    "style",
    "watch",
    "view",
    "sprites",
    "scripts",
    "templates"
]

require("./gulp-tasks")(tasks);
