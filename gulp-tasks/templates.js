var gulp = require("gulp"),
    templateCache = require("gulp-angular-templatecache");

gulp.task("templates", function(){
    gulp.src(files.templates)
        .pipe(templateCache({
            module: "actions"
        }))
        .pipe(gulp.dest(development.js))
})
