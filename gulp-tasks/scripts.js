var gulp = require("gulp"),
    concat = require("gulp-concat"),
    ngAnnotate = require("gulp-ng-annotate"),
    uglify = require("gulp-uglify");

gulp.task("scripts", function(){
    gulp.src(files.js)
        .pipe(concat("app.js"))
        .pipe(ngAnnotate())
        .pipe(uglify())
        .pipe(gulp.dest(production.js));
})
