var gulp = require("gulp");

module.exports = function(tasks) {
    /**
     * Цикл перебирает и вызывает все таски, объявленные в главном gulpfile
     */
    tasks.forEach(function(task){
        // gulp.task(task, require('./' + task));
        require("./" + task);
    })
}
