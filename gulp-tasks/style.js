var gulp = require("gulp"),
    sass = require("gulp-sass"),
    concat = require("gulp-concat"),
    autoprefixer = require("gulp-autoprefixer"),
    minifyCSS = require("gulp-minify-css");

gulp.task("style", function(){
    var styles = [
        files.scss.entryPoint,
        development.scss + "sprite.css"
    ];
    gulp.src(styles)
        .pipe(sass())
        .pipe(concat("main.css"))
        .pipe(autoprefixer())
        .pipe(minifyCSS())
        .pipe(gulp.dest(production.css))
})
