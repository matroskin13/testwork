var gulp = require("gulp"),
    spritesmith = require("gulp.spritesmith");

gulp.task("sprites", function(){
    var sprite = gulp.src(files.sprites)
        .pipe(spritesmith({
            imgName: 'sprite.png',
            cssName: 'sprite.css',
            imgPath: "../images/sprite.png",
            algorithm: 'binary-tree'
        }))
    sprite.img.pipe(gulp.dest(production.sprites));
    sprite.css.pipe(gulp.dest(development.scss));
})
