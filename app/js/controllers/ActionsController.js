angular.module("actions").controller("ActionsController", ActionsController);

function ActionsController(ActionsModel, ScoresModel) {
    this.scores = ScoresModel.get();
    this.actions = ActionsModel.get();
}
