angular.module("actions").directive("action", ActionDirective);

function ActionDirective() {
    var secondToFormat = function(second) {
        var minute;
        minute = Math.floor(second / 60);
        second = second - (minute * 60);
        if (second < 10) {
            second = "0" + second;
        }
        return minute + ":" + second;
    }
    return {
        templateUrl: "action.html",
        replace: true,
        scope: {
            action: "=config",
            scores: "=scores"
        },
        link: function($scope) {
            $scope.isBlock = false;
            $scope.timer = 0;
            $scope.timerFormat = "";
            $scope.iconClass = "icon-" + $scope.action.icon;

            $scope.clickHandler = function() {
                if ($scope.isBlock) {
                    return 0;
                }
                $scope.timer = $scope.action['recovery_time'];
                $scope.isBlock = true;
                $scope.scores = $scope.scores + $scope.action.points;
                $scope.timerFormat = secondToFormat($scope.timer);
                var timer = setInterval(function () {
                    $scope.timer = $scope.timer - 1;
                    if ($scope.timer === 0) {
                        $scope.isBlock = false;
                        clearTimeout(timer);
                    } else {
                        $scope.timerFormat = secondToFormat($scope.timer);
                    }
                    $scope.$apply();
                }, 1000);
            }
        }
    }
}
