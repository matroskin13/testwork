angular.module("actions").factory("ActionsModel", ActionsModel);

function ActionsModel($http) {
    var _actions = [
        {
            id: 145,
            'recovery_time': 600,
            points: 10,
            icon: "drink"
        },
        {
            id: 145,
            'recovery_time': 660,
            points: 20,
            icon: "meat"
        },
        {
            id: 145,
            'recovery_time': 480,
            points: 30,
            icon: "clock"
        },
        {
            id: 145,
            'recovery_time': 480,
            points: 40,
            icon: "storm"
        }
    ];
    return {
        get: function() {
            return _actions;
        },
        set: function(actions) {
            _actions = actions;
        }
    }
}
