angular.module("actions").factory("ScoresModel", ScoresModel);

function ScoresModel() {
    var _scores = 0;

    return {
        get: function() {
            return _scores;
        },
        set: function(scores) {
            _scores = scores;
        }
    }
}
